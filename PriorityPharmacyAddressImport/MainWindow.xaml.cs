﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Forms;
using IdsRemote;
using IdsRemoteService.IdsRemoteServiceReference;
using PriorityPharmacyAddressImport.Properties;
using RemoteService;
using RemoteService.Utils;
using Utilities.Csv;
using MessageBox = System.Windows.Forms.MessageBox;

namespace PriorityPharmacyAddressImport
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7;

        private const string RESELLER = "priority",
                             ACCOUNT = "WestBranch";

        private RemmoteService Rs;
        private bool LoggedIn;

        private void ImportLiveButton_Click( object sender, RoutedEventArgs e )
        {
            Globals.EndPoints.UseTestingServer = false;
            DoImport();
        }

        private void ImportTestingtButton_Click( object sender, RoutedEventArgs e )
        {
            Globals.EndPoints.UseTestingServer = true;
            DoImport();
        }

        private void EnableImport()
        {
            var Enab = LoggedIn && !string.IsNullOrEmpty( ( (string)FileName.Content ).Trim() );
            ImportTestingtButton.IsEnabled = Enab;
            ImportLiveButton.IsEnabled = Enab;
        }

        private void Button_Click( object sender, RoutedEventArgs e )
        {
            Globals.EndPoints.UseTestingServer = false;
            Rs = new RemmoteService( new IdsService() );
            var User = Rs.LogIn( RESELLER, UserName.Text.Trim(), Password.Password );
            if( User == null )
                MessageBox.Show( "Unable to sign-in", "Error", MessageBoxButtons.OK );
            else
            {
                {
                    MessageBox.Show( "Signed-in OK", "Signed-in", MessageBoxButtons.OK );
                    LoggedIn = true;
                    EnableImport();
                }
            }
        }


        public MainWindow()
        {
            ServicePointManager.DefaultConnectionLimit = 64;
            WindowState = WindowState.Minimized;
            InitializeComponent();

            ImportTestingtButton.IsEnabled = false;
            ImportLiveButton.IsEnabled = false;
        }

        private void BrowseButtom_Click( object sender, RoutedEventArgs e )
        {
            var FileDialog = new OpenFileDialog
                             {
                                 CheckFileExists = true,
                                 Filter = "csv files (*.csv)|*.csv"
                             };

            var Result = FileDialog.ShowDialog();
            switch( Result )
            {
            case System.Windows.Forms.DialogResult.OK:
                var File = FileDialog.FileName;
                FileName.Content = File;
                break;
            }
            EnableImport();
        }

        private void Window_Loaded( object sender, RoutedEventArgs e )
        {
            var Default = Settings.Default;
            FileName.Content = Default.LastFileImported;
            Top = Default.Top;
            Left = Default.Left;

            var H = Default.Height;
            if( H > 0 )
                Height = H;

            var W = Default.Width;
            if( W > 0 )
                Width = W;

            UserName.Text = Default.UserName;
            WindowState = WindowState.Normal;
        }

        private void Window_Closing( object sender, CancelEventArgs e )
        {
            var Default = Settings.Default;
            Default.LastFileImported = (string)FileName.Content;
            Default.Top = (int)Top;
            Default.Left = (int)Left;
            Default.Height = (int)Height;
            Default.Width = (int)Width;
            Default.UserName = UserName.Text;
            Default.Save();
        }

        private static string FixRgion( string region )
        {
            switch( region.Trim().ToUpper() )
            {
            case "MI":
                return "Michigan";
            default:
                return region;
            }
        }

        private void DoImport()
        {
            var ImportFile = ( (string)FileName.Content ).Trim();
            Csv Csv;
            try
            {
                Csv = new Csv( new FileStream( ImportFile, FileMode.Open, FileAccess.Read, FileShare.None ) );
            }
            catch( Exception Exception )
            {
                Console.WriteLine( Exception );
                MessageBox.Show( $"Unable to open import file. ({ImportFile})", "Error", MessageBoxButtons.OK );
                return;
            }
            var Rows = Csv.Rows;

            var Accounts = new HashSet<string>();
            for( var Index = 1; Index < Rows; Index++ ) // Skip Header
            {
                var Row = Csv[ Index ];
                Accounts.Add( Row[ A ].AsString.Trim() );
            }

            var AllAccounts = new HashSet<string>();
            foreach( var Acnt in Rs.GetAllAccounts() )
                AllAccounts.Add( Acnt.AccountId );

            var MissingAccounts = "";
            var First = true;
            foreach( var Account in Accounts )
            {
                if( !AllAccounts.Contains( Account ) )
                {
                    if( !First )
                        MissingAccounts += ", ";
                    else
                        First = false;
                    MissingAccounts += Account;
                }
            }

            if( !string.IsNullOrEmpty( MissingAccounts ) )
                MessageBox.Show( $"Unable to find import account(s). ({MissingAccounts})", "Error", MessageBoxButtons.OK );
            else
            {
                var Reseller = Rs.CurrentUser.ResellerId;

                var Addresses = new List<addCommonAddress>();

                for( var Index = 1; Index < Rows; Index++ ) // Skip Header
                {
                    var Row = Csv[ Index ];

                    var Account = Row[ A ].AsString.Trim();

                    Addresses.Add( new addCommonAddress
                                   {
                                       accountId = Account,
                                       authToken = Rs.AuthTokenAsString(),
                                       remoteCommonAddress = new remoteCommonAddress
                                                             {
                                                                 resellerId = Reseller,
                                                                 accountId = Account,
                                                                 description = Row[ B ].AsString.Trim(),
                                                                 suite = "",
                                                                 street = Row[ D ].AsString.Trim(),
                                                                 city = Row[ E ].AsString.Trim(),
                                                                 province = FixRgion( Row[ F ].AsString.Trim() ),
                                                                 country = "United States",
                                                                 contactName = Row[ C ].AsString.Trim(),
                                                                 contactPhone = Row[ H ].AsString.Trim()
                                                             }
                                   } );
                }
                new Tasks.Task<addCommonAddress>().Run( Addresses, address =>
                {
                    using( var Client = new IdsClient( Globals.EndPoints.UseTestingServer ) )
                    {
                        try
                        {
                            Client.addCommonAddress( address );
                        }
                        catch( Exception Exception )
                        {
                            Console.WriteLine( Exception );
                        }
                    }
                }, 10 );

                MessageBox.Show( "Import completed", "Completed", MessageBoxButtons.OK );
            }
        }
    }
}